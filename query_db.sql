-- Different queries for different scenarios 


-- show the customer renting bike with deviceid X
SELECT customers.name 
FROM customers 
INNER JOIN bikes
ON customers.cust_id = bikes.cust_id
WHERE bikes.deviceid = 'bike_batman_01';

-- show bike_id from deviceid
SELECT bike_id 
FROM bikes
WHERE deviceid = 'bike_batman_01';

-- show where customer name is located
SELECT gps_x, gps_y 
FROM iotdata
INNER JOIN bikes 
ON bikes.bike_id = iotdata.bike_id
INNER JOIN customers
ON customers.cust_id = bikes.cust_id 
WHERE customers.name = 'John Doe'
ORDER BY ts DESC LIMIT 1;

-- show where the bike with deviceid X is located
SELECT gps_x, gps_y 
FROM iotdata
INNER JOIN bikes
ON iotdata.bike_id = bikes.bike_id
WHERE bikes.deviceid = 'bike_batman_01'
ORDER BY ts DESC LIMIT 1;

-- show alert bikes
SELECT gps_x, gps_y 
FROM iotdata
WHERE status = 1;

-- show all bikes with their current customer
SELECT b.bike_id, b.deviceid, c.name, b.status, b.updated_at
FROM bikes as b
INNER JOIN customers as c
ON b.cust_id = c.cust_id;

-- show all cust_id and their bike, group by customer, get max date (last)
SELECT m.cust_id, m.name, m.deviceid, b.mx
FROM (
SELECT cust_id, max(updated_at) AS mx
FROM bikes 
GROUP BY cust_id
) b JOIN bikes m ON m.cust_id = b.cust_id AND b.mx = m.updated_at;

-- show all customers with their bikes, groupped by customer, get  max date
SELECT b1.deviceid, c.name as customer, b1.status, b1.updated_at
FROM (
SELECT m.cust_id, m.name, m.deviceid, m.status, m.updated_at, b.mx
FROM (
SELECT cust_id, max(updated_at) AS mx
FROM bikes 
GROUP BY cust_id
) b INNER JOIN bikes m ON m.cust_id = b.cust_id AND b.mx = m.updated_at
) b1 INNER JOIN customers AS c ON b1.cust_id = c.cust_id;

-- show bikes id and their last location
SELECT m.bike_id, m.gps_x, m.gps_y, m.status, iot.mx
FROM (
SELECT bike_id, max(ts) AS mx
FROM iotdata
GROUP BY bike_id
) iot INNER JOIN iotdata m ON m.bike_id = iot.bike_id AND iot.mx = m.ts;

-- show last location of bikes with their customer
SELECT f.bike_id, f.gps_x, f.gps_y, f.ts, c.name
FROM (
SELECT i.bike_id, k.cust_id, k.deviceid, i.gps_x, i.gps_y, i.ts
FROM (
SELECT m.bike_id, m.gps_x, m.gps_y, m.status, iot.mx, m.ts
FROM (
SELECT bike_id, max(ts) AS mx
FROM iotdata
GROUP BY bike_id
) iot INNER JOIN iotdata m ON m.bike_id = iot.bike_id AND iot.mx = m.ts
) i INNER JOIN bikes AS k ON i.bike_id = k.bike_id
) f INNER JOIN customers AS c ON f.cust_id = c.cust_id;

-- FIXED
SELECT f.bike_id, f.gps_x, f.gps_y, f.ts, f.status, c.name
FROM (
SELECT i.bike_id, k.cust_id, k.deviceid, i.gps_x, i.gps_y, i.status, i.ts
FROM (
SELECT m.bike_id, m.gps_x, m.gps_y, m.status, iot.mx, m.ts
FROM (
SELECT bike_id, max(ts) AS mx
FROM iotdata
GROUP BY bike_id
) iot INNER JOIN iotdata m ON m.bike_id = iot.bike_id AND iot.mx = m.ts
) i INNER JOIN bikes AS k ON i.bike_id = k.bike_id
) f INNER JOIN customers AS c ON f.cust_id = c.cust_id;

